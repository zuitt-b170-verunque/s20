// console.log(`Hello world`)

// let object = 
// {
//     city: "Ormoc",
//     province: "Leyte",
//     country: "Philippines"
// }

// console.log(`${object.city}, ${object.province}, ${object.country}`)

let object = 
{
    "city": "Ormoc",
    "province": "Leyte",
    "country": "Philippines"
}

console.log(`${object.city}, ${object.province}, ${object.country}`)
console.log(object);


let cities = [
    {
        "city": "Ormoc",
        "province": "Leyte",
        "country": "Philippines"
    },
    {
        "city": "Bacoor",
        "province": "Cavite",
        "country": "Philippines"
    },
    {
        "city": "New York",
        "province": "New York",
        "country": "Philippines"
    }
    ];
    console.log(cities);




let batches = [
    {
        "batchName": "Batch X"
    },
    {
        "batchName": "Batch Y"
    }
    ];
    console.log("Result from console.log");
    console.log(batches);

    console.log("Result from stringify method");
    console.log(JSON.stringify(batches));

let data = JSON.stringify({
    name: "John",
    age: 31,
    address:{
        city: "Manila",
        country:"Philippines"
    }
})
console.log(data);


/* 
Assignment
        create userDatails variable that will contain js object with the following properties
                fname - prompt
                lname - prompt
                age - prompt
                address:{
                        city - prompt
                        country - prompt
                        zipCode - prompt
                }
                        
        log in the console the converted JSON data type
*/

/* fname = prompt("What is your first name?");
lname =  prompt("What is your last name?");
age = prompt("How old are you");

city = prompt("What city are in?");
country = prompt("What Country?");
zipcode = prompt("The zipcode is?");

let user_details = JSON.stringify
(
    {
        first_name: fname,
        last_name: lname,
        age2: age,
        Address:{
            city2: city,
            country2: country,
            zipcode2: zipcode
        }
    }
)
console.log(user_details); */





// PARSE METHOD
let batchesJSON = 
`[
    {
        "batchName": "Batch X"
    },
    {
        "batchName": "Batch Y"
    }
]`
console.log(batchesJSON);
console.log("Result from parse method:");

// converting JSON data into JS Objects
console.log(JSON.parse(batchesJSON));


let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address":{
		"city": "Manila",
		"country": "Philippines"
	}
}`;

console.log(JSON.parse(stringifiedData));
